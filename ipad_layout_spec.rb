#-----------------------------------------------------------------------------------------------------------------
# ShopKeep - Ipad Layout Screen Test
# written by James Shiels 04/03/2016
# Purpose: ShopKeep automation Test - Test suite for Ipad layout screen.
#-----------------------------------------------------------------------------------------------------------------

# Load in all required items
$LOAD_PATH << '.'
require 'selenium-webdriver'
require 'Ipad_layout'

#initialise global variable driver to hold the instance of the web driver..
$driver = Selenium::WebDriver.for :firefox
$wait = Selenium::WebDriver::Wait.new(:timeout => 5) # seconds
$driver.manage.window.maximize

#initialise the ipad_layout class for use
ipad_layout = Ipad_layout.new

#carry out any initial setup required
ipad_layout.setup()


puts("-----------------------------------------------------------------------------------------------------------------")
puts("Beginning of test")
puts("-----------------------------------------------------------------------------------------------------------------")

puts("Setup")
ipad_layout.setup()

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 1 - Item search")
puts("-----------------------------------------------------------------------------------------------------------------")

#search for Misc Non-Taxable
text_to_search = "Misc"
ipad_layout.item_search(text_to_search)

#reset search by passing in blank value to search for.
text_to_search = ""
ipad_layout.item_search(text_to_search)

#search for Shipping
text_to_search = "Shipping"
ipad_layout.item_search(text_to_search)

#reset search by passing in blank value to search for.
text_to_search = ""
ipad_layout.item_search(text_to_search)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 2 - Create new page")
puts("-----------------------------------------------------------------------------------------------------------------")

ipad_layout.create_new_page

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 3 - delete page")
puts("-----------------------------------------------------------------------------------------------------------------")

#delete new page that was created above
page_to_delete = "New Page"
ipad_layout.delete_page(page_to_delete)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 4 - page naming")
puts("-----------------------------------------------------------------------------------------------------------------")

#highlight and rename page 1 -> Food
original_page_name = "Page 1"
new_page_name = "Food"
ipad_layout.change_page_name(original_page_name, new_page_name)


puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 5 - drag and drop item to page")
puts("-----------------------------------------------------------------------------------------------------------------")

#drag and drop 3 items from list onto page, and choose the position on page to place them
ipad_layout.drag_item_to_page("Bagel", "1")
ipad_layout.drag_item_to_page("Coffee", "2")
ipad_layout.drag_item_to_page("Latte", "3")

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 6 - remove item from page(page1)")
puts("-----------------------------------------------------------------------------------------------------------------")

#select item to remove from page, and remove it
name_of_button = "Latte"
ipad_layout.drag_button_to_list(name_of_button)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 7 - Move button position on page")
puts("-----------------------------------------------------------------------------------------------------------------")

#select button to move and select new position for that button
button_to_move = "Coffee"
new_button_position = "11"
ipad_layout.move_button_position(button_to_move, new_button_position)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 8 - change color of button")
puts("-----------------------------------------------------------------------------------------------------------------")

#select button to change color of, color change is defaulted
button_to_change = "Bagel"
ipad_layout.change_button_color(button_to_change)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 9 - change name of button")
puts("-----------------------------------------------------------------------------------------------------------------")

#select button to change name of, and pass in new name
original_name = "Coffee"
new_name = "Black Coffee"

ipad_layout.change_name_of_button(original_name, new_name)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 10 - navigation - with click")
puts("-----------------------------------------------------------------------------------------------------------------")

#create new page to allow multiple pages to navigate between
ipad_layout.create_new_page
#update new pages name
ipad_layout.change_page_name("New Page", "Misc")

#navigate between pages with clicks but clicking on the tabs
ipad_layout.navigate_pages_with_click("Food")
ipad_layout.navigate_pages_with_click("Misc")

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 11 - navigation - with buttons")
puts("-----------------------------------------------------------------------------------------------------------------")

#navigate using the previous and next buttons and either end of the tabs
ipad_layout.navigate_pages_with_buttons("Previous")
ipad_layout.navigate_pages_with_buttons("Next")

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 12 - drag and drop item to page (page2)")
puts("-----------------------------------------------------------------------------------------------------------------")

#same as test 5 only specific for page two
ipad_layout.navigate_pages_with_click("Misc")
ipad_layout.drag_item_to_page_two("Misc Non-Taxable", "1")
ipad_layout.drag_item_to_page_two("Misc Taxable", "2")
ipad_layout.drag_item_to_page_two("Muffin", "3")

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 13 - remove item from page(page2)")
puts("-----------------------------------------------------------------------------------------------------------------")

#same as test 6 only specific for page 2
name_of_button = "Muffin"
ipad_layout.drag_button_to_list(name_of_button)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 14 - move button position on page (page2)")
puts("-----------------------------------------------------------------------------------------------------------------")

#same as test 7 only specific for page 2
button_to_move = "Misc Taxable"
new_button_position = "11"
ipad_layout.move_button_position_page_two(button_to_move, new_button_position)

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 15 - change color of button (page2")
puts("-----------------------------------------------------------------------------------------------------------------")

#same as test 8 only specific for page 2
ipad_layout.change_button_color_page_two("Misc Non-Taxable")

puts("-----------------------------------------------------------------------------------------------------------------")
puts("Test 16 - change name of button (page2")
puts("-----------------------------------------------------------------------------------------------------------------")

#same as test 9 only specific for page 2
original_name = "Misc Taxable"
new_name = "Misc: Taxable"

ipad_layout.change_name_of_button_page_two(original_name, new_name)

puts("-----------------------------------------------------------------------------------------------------------------")

puts("Cleanup")
ipad_layout.clean_up

puts("-----------------------------------------------------------------------------------------------------------------")
puts("End of test")
puts("-----------------------------------------------------------------------------------------------------------------")
