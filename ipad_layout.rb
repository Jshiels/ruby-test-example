#-----------------------------------------------------------------------------------------------------------------
# ShopKeep - Ipad Layout Screen
# written by James Shiels 04/03/2016
# Purpose: ShopKeep automation Test - holding class for all functionality related to the Ipad layout screen
#-----------------------------------------------------------------------------------------------------------------

# Load in all required items
require 'selenium-webdriver'

# Class definition
class Ipad_layout
	#handle login and take the user to the ipad_layout page incase of none login etc.
	#although this is outside the remit of the automation test, it was needed for doing end to end test runs on the script, due to the browser opening with no session data therefore login was needed.
	def setup()
		
		#navigate to the ipad_layout screen
		$driver.navigate.to "https://www.shopkeepapp.com/ipad-layout" 

		#if user is not logged in, and redirected to login page, hand the login page
		if $driver.current_url == "https://www.shopkeepapp.com/login"

			#get all elements needed during this action
			username = $driver.find_element(:id, 'store_name')
			email = $driver.find_element(:id, 'login')
			password = $driver.find_element(:id, 'password')
			login_button = $driver.find_element(:id, 'submit')

			#carry out login
			username.send_keys("test-shiels")
			email.send_keys("iqbal+shiels@shopkeep.com")
			password.send_keys("Password12")
			login_button.click
		end 

		#assert_equal($driver.current_url == "https://www.shopkeepapp.com/ipad-layout" ,"Correct page has loaded for the test.")
		puts("Setup has completed")
	end 

	#handle the item search section
	def item_search(search_string)

		search_box = $driver.find_element(:id, "item-search-input")

		#clear text field
		search_box.clear

		#enter search text
		search_box.send_keys(search_string)

		#if search string matches any of the pre sets then verify the outcome of the searches target is found.
		if search_string == "Misc"
			found_element = $driver.find_element(:xpath,'//span[.="Misc Non-Taxable"]')
			found_element_two = $driver.find_element(:xpath,'//span[.="Misc Taxable"]')

		elsif search_string == "Shipping"
			found_element_three = $driver.find_element(:xpath,'//span[.="Shipping"]')

		else
			#if resetting search via sending in "", need to send enter to trigger the filter after the .clear has blanked the field as .clear does not trigger the search
			search_box.send_keys(:enter)

		end
		puts("Item search for '"+search_string+"'' has completed")

	end

	#handle the creation of a new page
	def create_new_page
	
		#click create new page button
		new_page_button = $driver.find_element(:id, "add-button-page")
		new_page_button.click

		#verify new item appears on tab row
		found_element = $wait.until { $driver.find_element(:xpath,'//span[.="New Page"]') }

		#found_element = $driver.find_element(:xpath,'//span[.="New Page"]')
		puts("Create new page has completed")
	end

	#handle the deletion of a page using the page name to target page to delete
	def delete_page(page_name)

		#highlight target page
		page_to_delete = $driver.find_element(:xpath,'//span[.="'+page_name+'"]')
		page_to_delete.click

		#click delete page button
		delete_button = $driver.find_element(:id,'destroy-button-page')
		delete_button.click

		#cancel the deletion
		cancel_button = $wait.until { $driver.find_element(:xpath,'/html/body/div[2]/div/div[1]/div/div[2]/button[1]') }
		cancel_button.click

		#delete again
		sleep(2)
		delete_button.click

		#confirm deletion
		confirm_button = $wait.until { $driver.find_element(:xpath,'/html/body/div[2]/div/div[1]/div/div[2]/button[2]') }
		confirm_button.click

		puts("Delete page has compelted for page: "+page_name)
				
	end

	#handle the renaming of a page, targetting with original anme and changing to new name
	def change_page_name(original_name, new_name)

		#highligh target page
		page_to_rename = $wait.until  { $driver.find_element(:xpath,'//span[.="'+original_name+'"]') }
		sleep(1)
		page_to_rename.click

		#clear page name text box
		page_title_box = $driver.find_element(:id, "button-page-name")
		page_title_box.click

		#clear text field
		page_title_box.clear

		#input new name to text box
		page_title_box.send_keys(new_name)
		page_title_box.send_keys(:enter)

		puts("Change page name has completed: " +original_name+" -> "+new_name)
		
	end

	#handle drag and drop from item list to page, using item name to target
	def drag_item_to_page(item_name, page_position)

		#get target element via xpath targetting the item name
		target_element = $driver.find_element(:xpath,'//span[.="'+item_name+'"]')

		#get target destination object via xpath
		xpath = "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div/div["+page_position+"]"
		target_destination = $driver.find_element(:xpath, xpath)
		
		#carry out drag and drop action.
		$driver.action.drag_and_drop(target_element, target_destination).perform

		#verify button of item anem exists on page
		verify = $wait.until { $driver.find_element(:xpath,'//p[.="'+item_name+'"]') }
		puts("Drag and drop item to page has completed for: "+item_name+ " to position: "+page_position)

	end 

	def drag_item_to_page_two(item_name, page_position)

		#get target element via xpath targetting the item name
		target_element = $driver.find_element(:xpath,'//span[.="'+item_name+'"]')

		#get target destination object via xpath

		xpath = "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div[2]/div["+page_position+"]"
		target_destination = $driver.find_element(:xpath, xpath)
		
		#carry out drag and drop action.
		$driver.action.drag_and_drop(target_element, target_destination).perform

		#verify button of item anem exists on page
		verify = $wait.until { $driver.find_element(:xpath,'//p[.="'+item_name+'"]') }
		puts("Drag and drop item to page two has completed for: "+item_name+ " to position: "+page_position)

	end 

	#handle dragging a button back onto the item list, ising the button name to target
	def drag_button_to_list(button_name)
			
		#get target button
		target_element = $driver.find_element(:xpath,'//p[.="'+button_name+'"]')

		#get target destination
		target_destination = $driver.find_element(:id, "item-list")

		#carry out action
		$driver.action.drag_and_drop(target_element, target_destination).perform

		#verify item not appears in list
		verify = $wait.until { $driver.find_element(:xpath,'//span[.="'+button_name+'"]') }

		puts("Drag button to list has completed for: "+button_name)

	end

	#handle the moving of a button, use the button name to target
	def move_button_position(button_to_move, new_button_position)
		
		#get target element via xpath targetting the item name
		target_element = $driver.find_element(:xpath,'//p[.="'+button_to_move+'"]')
	
		#get target destination object via xpath
		#generate xpath dependant on page number
		xpath = "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div/div["+new_button_position+"]"
		target_destination = $driver.find_element(:xpath, xpath)
		
		#carry out drag and drop action.
		$driver.action.drag_and_drop(target_element, target_destination).perform

		#verify button of item name exists on page
		#verify = $wait.until { $driver.find_element(:xpath,'//p[.="'+button_to_move+'"]') }
		puts("Move button position has completed for: "+button_to_move+" to new position:" + new_button_position)

	end

	#handle the moving of a button, use the button name to target
	def move_button_position_page_two(button_to_move, new_button_position)
		
		#get target element via xpath targetting the item name
		target_element = $driver.find_element(:xpath,'//p[.="'+button_to_move+'"]')
	
		#get target destination object via xpath
		#generate xpath dependant on page number
		xpath = "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div[2]/div["+new_button_position+"]"
		target_destination = $driver.find_element(:xpath, xpath)
		
		#carry out drag and drop action.
		$driver.action.drag_and_drop(target_element, target_destination).perform

		#verify button of item name exists on page
		verify = $wait.until { $driver.find_element(:xpath,'//p[.="'+button_to_move+'"]') }

		puts("Move button position(page two) has completed for: "+button_to_move+" to new position:" + new_button_position)
	end

	#handle changing of a buttons color, button name to target
	def change_button_color(button_to_change)

		#$driver.navigate.refresh
		#get target buttons
		target_button = $driver.find_element(:xpath,'//p[.="'+button_to_change+'"]')
		color_pallete = $wait.until { $driver.find_element(:xpath, "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div/div[31]/div/div[3]/div/div[2]/a[1]") }

		# click on target button
		target_button.click
		#click on color
		color_pallete.click

		puts("Change button color has completed for: " +button_to_change)
	end 

	#handle changing of a buttons color, button name to target
	def change_button_color_page_two(button_to_change)

		#issue with no finding the xpath correctly on page 2, seems to be resolved by carrying out a refresh and navigating back to the second page.
		$driver.navigate.refresh
		navigate_pages_with_click("Misc")
		
		#get target buttons
		target_button = $driver.find_element(:xpath,'//p[.="'+button_to_change+'"]')
		color_pallete = $wait.until { $driver.find_element(:xpath, "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div[2]/div[32]/div/div[3]/div/div[2]/a[1]") }

		# click on target button
		target_button.click
		#click on color
		color_pallete.click

		puts("Change button color(page two) has completed for: " +button_to_change)
	end 

	#handle changing name of a button, using button name to target
	def change_name_of_button(original_name, new_name)
		
		#get target button
		target_button = $driver.find_element(:xpath,'//p[.="'+original_name+'"]')

		# click on target button
		target_button.click

		# click color from pallete
		name_box = $wait.until { $driver.find_element(:xpath, "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div/div[32]/div/div[3]/div/div[1]/input") }
		name_box.clear
		name_box.send_keys(new_name)
		name_box.send_keys(:enter)

		puts("Change button name has completed for: " +original_name+" -> "+ new_name)

	end

	#handle changing name of a button, using button name to target
	def change_name_of_button_page_two(original_name, new_name)
		
		#get target button
		target_button = $driver.find_element(:xpath,'//p[.="'+original_name+'"]')

		# click on target button
		target_button.click

		# click color from pallete
		name_box = $wait.until { $driver.find_element(:xpath, "/html/body/div[1]/div[4]/div[3]/div/div[3]/div/div[2]/div[31]/div/div[3]/div/div[1]/input") }

		name_box.clear
		name_box.send_keys(new_name)
		name_box.send_keys(:enter)

		puts("Change button name has completed for: " +original_name+" -> "+ new_name)

	end

	#handle navigation of page tabs with mouse clicks, using page name to target
	def navigate_pages_with_click(page_name)

		#get target page tab
		page_to_rename = $wait.until  { $driver.find_element(:xpath,'//span[.="'+page_name+'"]') }
		sleep(1)
		
		#click target page tab
		page_to_rename.click
	
		#sleep to allow animation to complete before attempting next actions
		sleep(1)

		puts("Navigate with click has completed for: " +page_name)
 
	end

	#handle navigation of pages with the to start and to end buttons, parameter to decide which
	def navigate_pages_with_buttons(previous_or_next)
		
		#get if to_end or to_start from paramenter
		if previous_or_next == "Previous"
			#click << button
			target_button = $driver.find_element(:id,'button-tab-prev')
			target_button.click
		else 
			#click >> button
			target_button = $driver.find_element(:id,'button-tab-next')
			target_button.click
		end

		#sleep to allow animation to complete before attempting next actions
		sleep(1)

		puts("Navigate with buttons has completed for: " +previous_or_next)
	end

	def clean_up

		#close broswer for end of test
		$driver.close
		puts("Clean up has completed")
		
	end

end
